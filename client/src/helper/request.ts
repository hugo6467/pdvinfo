import axios from 'axios'

// todo: pegar das envs
const BASE_URL = 'http://localhost:3000' // process.env.BASE_URL

const cookieStore = (window as any).cookieStore

const getUrl = (url: string): string => {
  return /https?:\/\/.+/i.test(url) ? url : `${BASE_URL}${url}`
}

const getHeaders = async (headers = {}) => {
  if (cookieStore) {
    const cookie = await cookieStore.get('authentication-token')
    return {
      ...(cookie ? { 'authentication-token': cookie.value } : {}),
      ...headers
    }
  }
}

export const requestHelper = {
  async get (url: string, params?: any, headers = {}) {
    return (
      await axios.get(
        getUrl(url),
        { params, headers: getHeaders(headers) }
      )
    ).data
  },
  async post (url?: string | any, params?: any, headers = {}, fakeCall?: boolean) {
    if (url) {
      return (
        await axios.post(
          getUrl(url),
          params, { headers: await getHeaders(headers) }
        )
      ).data
    } else if (fakeCall) {
      return {
        data: {
          value: ['fakeResponse']
        },
        status: 200,
        statusText: 'OK',
        headers: {},
        config: {},
        request: {}
      }
    }
  },
  async put (url: string, params?: any, headers = {}) {
    return (
      await axios.put(
        getUrl(url),
        params, { headers: getHeaders(headers) }
      )
    ).data
  },
  async delete (url: string, params?: any, headers = {}) {
    return (
      await axios.delete(
        getUrl(url),
        { params, headers: getHeaders(headers) }
      )
    ).data
  }
}
