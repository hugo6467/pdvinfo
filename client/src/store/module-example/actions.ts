import { ActionTree } from 'vuex'
import { StateInterface } from '../index'
import { ExampleStateInterface } from './state'

const actions: ActionTree<ExampleStateInterface, StateInterface> = {
  someAction (ctx, payload) {
    ctx.commit('someMutation', payload)
  }
}

export default actions
