import { MutationTree } from 'vuex'
import { ExampleStateInterface } from './state'

const mutation: MutationTree<ExampleStateInterface> = {
  someMutation (state: ExampleStateInterface, payload:any) {
    state.test = payload
  }
}

export default mutation
