export interface ExampleStateInterface {
  prop: boolean;
  test: string
}

function state (): ExampleStateInterface {
  return {
    prop: false,
    test: ''
  }
}

export default state
